package cn.uncode.baas.server.internal.log;

import java.util.Map;

import cn.uncode.baas.server.utils.DataUtils;
import cn.uncode.dal.utils.JsonUtils;


public class Logger {

    private static final ThreadLocal<StringBuffer> logValue = new ThreadLocal<StringBuffer>();

    private static final Logger LOG = new Logger();

    private Logger() {
    }

    public static Logger getInstance() {
        return LOG;
    }

    public static void init() {
        logValue.set(new StringBuffer());
    }

    public void info(Object msg) {
        if (msg instanceof String) {
            logValue.get().append(msg);
        } else if (msg instanceof Map) {
            logValue.get().append(DataUtils.convert2Map(msg));
        } else{
        	logValue.get().append(JsonUtils.objToMap(msg));
        }
    }

    public static String getValue() {
        return logValue.get().toString();
    }

}
